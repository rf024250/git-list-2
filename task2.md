cd ~ 
Explained: Change directory to the user's home directory
git init portfolio 
Explained: Initializes a new Git repository named 'portfolio' in the current directory
cd portfolio 
Explained: Changes the current directory to the 'portfolio' directory created in step 2.
ls -al 
Explained: Lists all files and directories in the current directory, including hidden files, with detailed information.
git status 
Explained: Displays the current status of the Git repository, showing tracked/untracked files and any modifications.
echo hello > .gitignore 
Explained: creates a file named .gitignore with the content "hello" (overwriting any existing content) to specify files/folders Git should ignore.
git add -A 
Explained: Adds all changes (new, modified, deleted files) in the repository to the staging area for the next commit.
git status 
Explained: Checks the status of the repository after staging changes with git add.
git config --global user.email “<student_email>” 
Explained: Sets the global email configuration for Git with the provided student email address.
git config –global user.name “<student_name>” 
Explained: Sets the global username configuration for Git with the provided student name.
git commit -m “first commit, adding week 1 content” 
Explained: Creates a commit with a message describing the changes made since the last commit.
git status 
Explained: Checks the status of the repository after committing changes.
git push 
Explained: Pushes committed changes to the remote repository (origin) associated with the branch to synchronize changes.
git remote add origin https://csgitlab.reading.ac.uk/<student_id>/cs1pc20_portfolio.git
Explained: Adds a remote orgin of website
git push –set-upstream origin master 
Explained: Push changes to the 'master' branch on the remote repository.
git status 
Explained: Check the status of the repository after pushing changes.
echo “# CS1PC20 Portfolio” > readme.md 
Explained: Create a readme.md file with a specific content.
git add readme.md 
Explained: Add the readme.md file to the staging area.
git commit -m “added readme file” 
Explained: Commit changes to the repository with a message.
git push 
Explained: Push committed changes to the remote repository.
git config –global credential.helper cache 
Explained: Cache Git credentials globally
git branch week2
Explained: Create a new branch named 'week2'
git checkout week2 
Explained: Switch to the 'week2' branch.
mkdir week2 
Explained: Create a new directory named 'week2'.
echo “# Week 2 exercise observations” > week2/report.md 
Explained: Create a report.md file in the 'week2' directory.
git status 
Explained: Check the status of the repository after making changes.
git add week2 
Explained: Add the 'week2' directory to the staging area.
git commit -m “added week 2 folder and report.md” 
Explained: Commit changes with a descriptive message.
git push 
Explained:  Push committed changes to the remote repository
git checkout master 
Explained: Switch back to the 'master' branch.
ls -al 
Explained: List all files and directories in the current directory.
git checkout week2 
Explained: Switch to the 'week2' branch.
ls -al 
Explained: Switch to the 'week2' branch.
git checkout master 
Explained: Switch back to the 'master' branch.
git merge week2 
Explained: Merge changes from the 'week2' branch into the 'master' branch.
ls -al 
Explained: List all files and directories after merging changes.
git push 
Explained: Push the merged changes to the remote repository. 
rm -r week2 
Explained: Remove the 'week2' directory recursively.
rm -r week1 
Explained: Remove the 'week1' directory recursively. 
ls -al 
Explained: List all files and directories after removing 'week1' and 'week2'.
git status 
Explained: Check the status of the repository after removing directories.
git stash 
Explained: Stash changes in the working directory.
git stash drop 
Explained: Remove the most recent stash of changes.
ls -al  
Explained: List all files and directories after stashing and dropping changes.
cd ~ 
Explained: Change directory to the user's home directory.
cp -r portfolio portfolio_backup 
Explained: Copy the 'portfolio' directory to 'portfolio_backup'.
rm -rf portfolio 
Explained: Remove the 'portfolio' directory forcibly and recursively.
ls -al 
Explained: List all files and directories after removing the 'portfolio' directory.
git clone https://csgitlab.reading.ac.uk/<rf024250>/cs1pc20_portfolio portfolio
Explained: Clone a remote repository into a local directory named 'portfolio'.
ls -al 
Explained:  List all files and directories in the current directory after cloning the repository.
